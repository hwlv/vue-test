import { config } from '@vue/test-utils'

config.mocks['setLabel'] = label => 'label'

// 定义全局方法。相当于window.setLabel
global.setLabel = () => {
  console.log('window.setLabel callback...')
}
