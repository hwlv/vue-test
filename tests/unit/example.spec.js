import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import ElementUI from 'element-ui'
// 引入测试的组件
import Foo from '@/components/Foo.vue'

// 引入vue实例，使用element-ui插件，具体由项目而定
const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)
localVue.use(ElementUI)

const routes = [{ path: '/foo', component: Foo }]
const router = new VueRouter({
  routes
})

// console.log('window.setLabel')
// console.log(window.setLabel)

// Mock store，有的组件会用到this.$store.state.tk。
let store = new Vuex.Store({
  state: {tk:'233232323',um:'ex-lvhongwang002'},
  actions:{
    actionClick: jest.fn(),
    actionInput: jest.fn()
  }
})

// 工厂方法
const factory = (values = {}) => {
  //  mount和shallowMount是2中组件挂载方式，创建一个包含被挂载和渲染的Vue组件的wrapper。
  //  不同的是shallowMount不会渲染子组件，只保留它们的存根，避免了子组件可能带来的副作用（比如Http请求等）。
  return shallowMount(Foo, {
    // 这里挂载的选项将会合并到测试组件中
    // 更多挂载选项在这里：https://vue-test-utils.vuejs.org/zh/api/options.html
    localVue,
    store,
    router,
    // 为实例添加额外的属性。在伪造全局注入的时候有用。
    mocks: {
    },
    // 设置组件实例的 $listeners 对象。
    listeners:{},
    propsData: {},
    // 将子组件存根。比如待测试组件里嵌套有其他组件不想被测试，可以通过stubs给去掉
    // stubs:{},
    data() {
      return {
        ...values
      }
    }
  })
}

// describe块称为"测试套件"（test suite），表示一组相关的测试。
// 它是一个函数，第一个参数是测试套件的名称（"组件Foo的测试"），第二个参数是一个实际执行的函数。
describe('Foo', () => {
  console.log('ENVVAR=' + process.env.ENVVAR)
  console.log('NODE_ENV=' + process.env.NODE_ENV)
  // it块称为"测试用例"（test case），表示一个单独的测试，是测试的最小单位。
  // 它也是一个函数，第一个参数是测试用例的名称（"renders a welcome message"），第二个参数是一个实际执行的函数。
  it('renders a welcome message', async () => {
    const wrapper = factory()
    // console.log('get label')
    
    // wrapper.vm可以拿到组件实例，可以打印一下wrapper.vm看看具体有哪些方法.
    // 1. 通过wrapper.vm.message拿到data的message值
    // 2. 用 setData 或 setProps 方法直接操作组件状态。
    // 3. setMethods({'add': ()=>{}}) 可以替代原组件方法
    // 4. find相当于document.querySelector

    // 但是获取更新的dom要通过await 或者 await wrapper.vm.$nextTick()。
    // console.log(wrapper.vm.message); // ''
    await wrapper.setData({ message: 'Welcome' })
    // console.log(wrapper.vm.message); // 'Welcome'
    await wrapper.vm.$nextTick()
    console.log(wrapper.find('#tk').text())
    console.log(wrapper.vm.$store.state.tk)

    // find方法对应querySelector和querySelectorAll。
    // 认证该组件渲染出来的 HTML 符合预期。
    expect(wrapper.find('.message').text()).toEqual('Welcome')
  })

  // expect是个断言函数，jest断言具体使用方法可以到https://jestjs.io/docs/en/expect
  it('assert test', () => {
    expect(4 + 5).toBe(9)
    expect(undefined).toBeUndefined()
    expect(null).toBeNull()
    expect(3).toBeTruthy() // 真值，和toBeFalsy相反
    expect(null).toBeFalsy() // 假值：false, 0, '', null, undefined, and NaN
    expect(Number('a')).toBeNaN()
    expect('grapefruits').toMatch('fruit')
    expect({ name }).toEqual({ name }) // true
  })

  it('renders an error when username is less than 7 characters', () => {
    const wrapper = factory({ username: '' })
    expect(wrapper.find('.error').exists()).toBeTruthy()
  })

  // 模拟用户交互，点击按钮数字加1
  it('button click should increment the count', async () => {
    const wrapper = factory()
    expect(wrapper.vm.count).toBe(0)
    const button = wrapper.find('button')
    const getText = () => wrapper.find('button').text()
    // 触发点击事件
    button.trigger('click')
    // console.log(wrapper.vm.count); // 1
    // 因为 Vue 会对未生效的 DOM 进行批量异步更新，避免因数据反复变化而导致不必要的渲染。
    // 所以同步代码拿不到异步更新的dom内容
    // console.log(getText()); // 0

    // 1. 用await拿到异步更新dom
    await button.trigger('click')
    console.log(getText()) //2

    // 2. 或者通过then方法
    button.trigger('click').then(() => {
      console.log(getText()) //3
    })

    // 断言
    expect(wrapper.vm.count).toBe(3)
    expect('wrapper.vm.count').toMatch('count')
    // expect(wrapper.vm.$el).toMatchSnapshot()
  })

  // 为App的单元测试增加快照（snapshot）：
  it('has the expected html structure', () => {
    const wrapper = factory();
    expect(wrapper.vm.$el).toMatchSnapshot()
  })
  // 测试表单提交
})

