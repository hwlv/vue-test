import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
// 引入测试的图表组件
import Chart from '@/components/Chart.vue'

describe('Test Chart Conponents', () => {
  it('renders canvas', async () => {
    let data={
      xData:['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
      seriesData:[820, 932, 901, 934, 1290, 1330, 1320]
    }
    const wrapper = mount(Chart, {
      // 属性赋值
      propsData: {data}
    })
    // 获取处理后的值
    console.log(wrapper.vm.lineOption)
    // 获取echarts生成的canvas dom元素
    const canvas = wrapper.find('canvas').element
    // 如果成功渲染，将会生成canvas并带有width属性
    console.log(canvas.width)
    // 在__snapshots__查看快照，认证该组件渲染出来的 HTML 符合预期。
    expect(wrapper.vm.$el).toMatchSnapshot()
  })
})
