const { defaults } = require('jest-config')

// defaults包括默认配置项及默认值
// jest.config.js 具体配置文件可查看文档：https://jestjs.io/docs/en/configuration#preset-string
// console.log(defaults)

module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  // 告诉jest需要解析的文件
  moduleFileExtensions: ['js', 'json', 'jsx', 'ts', 'tsx', 'node'],
  // 告诉jest去哪里找模块资源，同webpack中的modules
  moduleDirectories: ['src', 'node_modules'],
  // 开启测试报告，默认关闭。生成报告会降低单测的速度，保存在根目录的 coverage 文件夹下。
  collectCoverage: true,
  // 别名，同webpack中的alias
  // moduleNameMapper: {
  //   '^src(.*)$': '<rootDir>/src/$1',
  // },
  // 告诉jest去哪里找我们编写的测试文件，他匹配
  // 1. 任何 tests/unit 中以 .spec.(js|jsx|ts|tsx) 结尾的文件；
  // 2. 任何 __tests__ 目录中的 js(x)/ts(x) 文件。
  testMatch: ['**/__tests__/**/*.[jt]s?(x)', '**/?(*.)+(spec|test).[tj]s?(x)'],
  // 告诉jest在编辑的过程中可以忽略哪些文件，默认为node_modules下的所有文件。
  // transformIgnorePatterns: [
  //   '<rootDir>/node_modules/' +
  //     '(?!(vue-awesome|veui|resize-detector|froala-editor|echarts|vue-echarts|html2canvas|jspdf))'
  // ],
  // 在执行测试用例之前需要先执行的文件
  // 1. jest.init.js 可以用来定义一些全局方法
  // 2. jest-canvas-mock 补充jest dom canvas API不全的问题。如果有组件涉及canvas需要安装，不然会报错。
  setupFiles: ['./jest.init.js', 'jest-canvas-mock']
}
